AbandonedRebelBaseScreenPlay = ScreenPlay:new {
	numberOfActs = 1,

	screenplayName = "AbandonedRebelBaseScreenPlay",

	planet = "dantooine",

	mobiles = {
		--inside perimeter near east building area
		{"rebel_commando", 400, -6656.4, 30.0, 5552.4, -1, 0, ""},
		{"rebel_commando", 400, -6661.4, 30.0, 5557.4, -1, 0, ""},
		{"rebel_commando", 400, -6651.4, 30.0, 5547.4, -1, 0, ""},
		{"rebel_commando", 400, -6663.4, 30.0, 5562.4, -1, 0, ""},

		{"rebel_recruiter", 0, -6817, 46, 5511, 174, 0, ""},

		--inside main building
		{"vrovel", 60, -1.1, 1.0, 0.5, 177, 6555560, ""},
		{"ezkiel", 60, -3.2, 1.0, 7.9, 11, 6555560, ""}
	}
}

registerScreenPlay("AbandonedRebelBaseScreenPlay", true)

function AbandonedRebelBaseScreenPlay:start()
	if (isZoneEnabled(self.planet)) then
		self:spawnMobiles()
	end
end

function AbandonedRebelBaseScreenPlay:spawnMobiles()
	--inside perimeter near east building area
	spawnMobile("dantooine", "cis_battle_droid_commando", 400, -6656.4, 30.0, 5552.4, -1, 0)
	spawnMobile("dantooine", "cis_battle_droid_commando", 400, -6661.4, 30.0, 5557.4, -1, 0)
	spawnMobile("dantooine", "cis_battle_droid_commando", 400, -6651.4, 30.0, 5547.4, -1, 0)
	spawnMobile("dantooine", "cis_battle_droid_commando", 400, -6663.4, 30.0, 5562.4, -1, 0)

	spawnMobile("dantooine", "rebel_recruiter", 0, -6817, 46, 5511, 174, 0)
end
