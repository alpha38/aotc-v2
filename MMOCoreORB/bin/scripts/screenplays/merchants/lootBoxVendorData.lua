LootBoxVendorLogic = VendorLogic:new {
	scriptName = "LootBoxVendorLogic",
	currencies = {
	--For Tokens: Displayed Name, full template string (without shared_), if applicable: ScreenPlayData string, ScreenPlayData key
		{currency = "credits", name = "Credits", template = "", ScreenPlayDataString = "credits", ScreenPlayDataKey = "credits"},
		{currency = "token", name = "PVE Tokens", template = "object/tangible/loot/misc/event_token_pve.iff", ScreenPlayDataString = "vendorToken", ScreenPlayDataKey = "event_token"},
	},
	--Displayed Name, full template string (without shared_), cost in {}, use the same structure as currencies
	merchandise = {
		{name = "A Random Loot Crate", template = "loot_box_01", cost = {250000, 0}},
		{name = "Rare Colour Crystal", template = "named_color_crystals", cost = {0, 1}},
		{name = "Random Painting", template = "all_paintings", cost = {0, 1}},
		{name = "A Crate of Resources(1000)", template = "object/tangible/veteran_reward/resource_supply_crate.iff", cost = {0, 1}},
		{name = "Krayt Dragon Scales", template = "krayt_scales", cost = {0, 1}},
		{name = "Krayt Dragon Rare Tissue", template = "krayt_tissue_rare", cost = {0, 1}},
		{name = "Vibrovis", template = "vibrovis", cost = {0, 1}},
		{name = "Nightsister Vibro Units", template = "ns_vibros", cost = {0, 1}},
		{name = "Power Crystal", template = "power_crystals", cost = {0, 1}},
		{name = "Krayt Dragon Pearl", template = "krayt_pearls", cost = {0, 1}},
	},
}

registerScreenPlay("LootBoxVendorLogic", false)
