local ObjectManager = require("managers.object.object_manager")

TalusDetainmentCenterScreenPlay = ScreenPlay:new {
	numberOfActs = 1,

	screenplayName = "TalusDetainmentCenterScreenPlay",

	buildingID = 9686208,

--9686208 bunker imperial detainment center
--9686346 terminal hq
--9686357 override terminal
--9686389 power regulator
--9686393 security terminal
--9686398 uplink terminal
}

registerScreenPlay("TalusDetainmentCenterScreenPlay", true)

function TalusDetainmentCenterScreenPlay:start()
	if (isZoneEnabled("talus")) then
		local pBuilding = getSceneObject(self.buildingID)
		createObserver(FACTIONBASEFLIPPED, "TalusDetainmentCenterScreenPlay", "flipBase", pBuilding)

		if getRandomNumber(100) >= 50 then
			self:spawnRebels(pBuilding)
		else
			self:spawnImperials(pBuilding)
		end
	end
end

function TalusDetainmentCenterScreenPlay:flipBase(pBuilding)
	if (pBuilding == nil) then
		return 1
	end

	BuildingObject(pBuilding):destroyChildObjects()

	if BuildingObject(pBuilding):getFaction() == FACTIONIMPERIAL then
		self:spawnRebels(pBuilding)
	elseif BuildingObject(pBuilding):getFaction() == FACTIONREBEL then
		self:spawnImperials(pBuilding)
	end

	return 0
end

function TalusDetainmentCenterScreenPlay:spawnImperials(pBuilding)
	BuildingObject(pBuilding):initializeStaticGCWBase(FACTIONIMPERIAL)

	BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_imperial.iff", 3.5, -9.0, 35.1, 9686212, 1, 0, 0, 0)

	BuildingObject(pBuilding):spawnChildCreature("imperial_staff_corporal", 180, 3.5, -9.0, 32.6, 179, 9686212)
	BuildingObject(pBuilding):spawnChildCreature("command_security_guard", 180, 4.8, -9.0, 35.1, 179, 9686212)
	BuildingObject(pBuilding):spawnChildCreature("command_security_guard", 180, 2.2, -9.0, 35.1, 179, 9686212)
	BuildingObject(pBuilding):spawnChildCreature("imperial_warrant_officer_i", 180, -52.6, -9.0, -6.6, -88, 9686214)
	BuildingObject(pBuilding):spawnChildCreature("imperial_private", 180, -58.4, -9.0, 7.1, 150, 9686214)
	BuildingObject(pBuilding):spawnChildCreature("imperial_noncom", 180, -72.9, -9.0, 21.2,  165, 9686214)
	BuildingObject(pBuilding):spawnChildCreature("imperial_staff_corporal", 180, -54.5, -9.0, 67.1, 179, 9686214)
	BuildingObject(pBuilding):spawnChildCreature("imperial_noncom", 180, -52.3, -9.0, 63.2, 90, 9686214)
	BuildingObject(pBuilding):spawnChildCreature("imperial_noncom", 180, -53.2, -9.0, 59.0, 90, 9686214)
	BuildingObject(pBuilding):spawnChildCreature("imperial_noncom", 180, -57.0, -9.0, 60.9, -120, 9686214)
	BuildingObject(pBuilding):spawnChildCreature("command_security_guard", 180, -24.9, -9.0, 28.5, 90, 9686213)
	BuildingObject(pBuilding):spawnChildCreature("command_security_guard", 180, -24.8, -9.0, 26.0, 90, 9686213)
	BuildingObject(pBuilding):spawnChildCreature("command_security_guard", 180, 12.1, -17.0, 65.0, 179, 9686215)
	BuildingObject(pBuilding):spawnChildCreature("command_security_guard", 180, 12.1, -17.0, 62.0, -1, 9686215)
	BuildingObject(pBuilding):spawnChildCreature("clonetrooper", 180, 30.4, -17.0, 60.7, 1, 9686215)
	BuildingObject(pBuilding):spawnChildCreature("clonetrooper", 180, 30.4, -17.0, 65.9, 179, 9686215)
	BuildingObject(pBuilding):spawnChildCreature("clonetrooper_captain", 180, 34.5, -17.0, 63.5, -90, 9686215)
	BuildingObject(pBuilding):spawnChildCreature("clone_commando", 180, -1.2, -23.0, 225.4, 179, 9686222)
	BuildingObject(pBuilding):spawnChildCreature("clone_commando", 180, -3.8, -23.0, 225.4, 179, 9686222)
	BuildingObject(pBuilding):spawnChildCreature("imperial_first_lieutenant", 180, -19.8, -23.0, 234.4, 179, 9686222)
	BuildingObject(pBuilding):spawnChildCreature("clonetrooper", 180, 1.0, -23.0, 192.3, 90, 9686217)
	BuildingObject(pBuilding):spawnChildCreature("clonetrooper", 180, -2.0, -23.0, 192.3, 90, 9686217)
	BuildingObject(pBuilding):spawnChildCreature("clonetrooper", 180, -5.0, -23.0, 192.3, 90, 9686217)
	BuildingObject(pBuilding):spawnChildCreature("clonetrooper", 180, -8.0, -23.0, 192.3, 90, 9686217)
	BuildingObject(pBuilding):spawnChildCreature("clonetrooper", 180, 1.0, -23.0, 196.3, 90, 9686217)
	BuildingObject(pBuilding):spawnChildCreature("clonetrooper", 180, -2.0, -23.0, 196.3, 90, 9686217)
	BuildingObject(pBuilding):spawnChildCreature("clonetrooper", 180, -5.0, -23.0, 196.3, 90, 9686217)
	BuildingObject(pBuilding):spawnChildCreature("clonetrooper", 180, -8.0, -23.0, 196.3, 90, 9686217)
	BuildingObject(pBuilding):spawnChildCreature("imperial_first_lieutenant", 300, 7.1, -23.0, 198.5, -135, 9686217)
	BuildingObject(pBuilding):spawnChildCreature("rep_jedi_master", 300, -80.5, -23.0, 259.4, 88, 9686223)
	BuildingObject(pBuilding):spawnChildCreature("rep_jedi_knight", 300, 45.2, -23.0, 276.3, -179, 9686225)
	BuildingObject(pBuilding):spawnChildCreature("rep_jedi_knight", 1200, -2.7, -23.0, 268.5, 90, 9686224)
end

function TalusDetainmentCenterScreenPlay:spawnRebels(pBuilding)
	BuildingObject(pBuilding):initializeStaticGCWBase(FACTIONREBEL)

	BuildingObject(pBuilding):spawnChildSceneObject("object/tangible/gcw/flip_banner_onpole_rebel.iff", 3.5, -9.0, 35.1, 9686212, 1, 0, 0, 0)

	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_squad_leader", 180, 5.3, -9.0, 29.7, -1, 9686212)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 180, 6.9, -9.0, 33.9, 0, 9686212)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 180, 0.0, -9.0, 33.9, -6, 9686212)
	BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 180, -48.1, -9.0, -6.6, 90, 9686214)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_squad_leader", 180, -58.4, -9.0, 3.2, 0, 9686214)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_specforce", 180, -78.2, -9.0, 21.5,  173, 9686214)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_squad_leader", 180, -52.0, -9.0, 61.6, 88, 9686214)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_specforce", 180, -57.0, -9.0, 58.7, -90, 9686214)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_specforce", 180, -57.0, -9.0, 47.0, -96, 9686214)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_specforce", 180, -52.0, -9.0, 43.3, 91, 9686214)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 180, -47.0, -9.0, 26.0, -176, 9686213)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 180, -31.2, -9.0, 28.8, -2, 9686213)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 180, 14.5, -17.0, 59.8, 85, 9686215)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 180, 26.0, -17.0, 67.0, 73, 9686215)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 180, 25.0, -17.0, 58.8, -125, 9686215)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 180, 34.2, -17.0, 66.9, 4, 9686215)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_captain", 180, 34.6, -17.0, 61.0, 98, 9686215)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_specforce", 180, -17.3, -23.0, 227.3, 168, 9686222)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid_specforce", 180, -18.0, -23.0, 232.7, -102, 9686222)
	BuildingObject(pBuilding):spawnChildCreature("cis_magnaguard", 180, -25.5, -23.0, 233.4, -93, 9686222)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 180, 14.0, -23.0, 192.3, 0, 9686217)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 180, 14.0, -23.0, 189.3, 0, 9686217)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 180, 14.0, -23.0, 186.3, 0, 9686217)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 180, 14.0, -23.0, 183.3, 0, 9686217)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 180, 10.0, -23.0, 192.3, 0, 9686217)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 180, 10.0, -23.0, 189.3, 0, 9686217)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 180, 10.0, -23.0, 186.3, 0, 9686217)
	BuildingObject(pBuilding):spawnChildCreature("cis_battle_droid", 180, 10.0, -23.0, 183.3, 0, 9686217)
	BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 300, 6.8, -23.0, 203.1, -9, 9686217)
	BuildingObject(pBuilding):spawnChildCreature("cis_sbd", 300, -79.8, -23.0, 259.1, 89, 9686223)
	BuildingObject(pBuilding):spawnChildCreature("specops_alliance_free_agent", 300, 43.2, -23.0, 278.3, -177, 9686225)
	BuildingObject(pBuilding):spawnChildCreature("cis_magnaguard", 1200, -3.7, -23.0, 269.5, 90, 9686224)
end
