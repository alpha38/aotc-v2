-- AOTC - Alpha - 2024

restussPointControl = ScreenPlay:new {
	scriptName = "restussPointControl",
	planetName = "rori",
	x1 = 5061,
	z1 = 79,
	y1 = 5746,
	
	x2 = 5386,
	z2 = 80,
	y2 = 5644,
	
	x3 = 5562,
	z3 = 80,
	y3 = 5585,

	bannerNeutral = "object/tangible/furniture/all/event_flag_game_neut_banner.iff",
	bannerSep = "object/tangible/furniture/all/event_flag_game_reb_banner.iff",
	bannerRep = "object/tangible/furniture/all/event_flag_game_imp_banner.iff",
	radius = 25,
}

registerScreenPlay("restussPointControl", true)

function restussPointControl:start()
	-- Clear it out first
	self:stop()
	createEvent(5 * 1000, self.scriptName, "startEncounter", nil, "")
end

function restussPointControl:stop()
	writeData("restussPointControl:despawn", 1)
	createEvent(1 * 1000, self.scriptName, "cleanupScene", nil, "")
end

function restussPointControl:startEncounter()
	-- Clear it out first
	writeData("restussPointControl:despawn", 0)
	
	broadcastGalaxy("all", "The Battle for Restuss Begins Again.")

	pSector1 = spawnSceneObject(self.planetName, self.bannerNeutral, self.x1, self.z1, self.y1, 0, 90)
	writeData("restussPointControl:pSector1", SceneObject(pSector1):getObjectID())
	self:storeObjectID(pSector1)
	
	pSector2 = spawnSceneObject(self.planetName, self.bannerNeutral, self.x2, self.z2, self.y2, 0, 90)
	writeData("restussPointControl:pSector2", SceneObject(pSector2):getObjectID())
	self:storeObjectID(pSector2)
	
	pSector3 = spawnSceneObject(self.planetName, self.bannerNeutral, self.x3, self.z3, self.y3, 0, 90)
	writeData("restussPointControl:pSector3", SceneObject(pSector3):getObjectID())
	self:storeObjectID(pSector3)
		
	writeData("restussPointControl:sector1Score", 0)
	writeData("restussPointControl:sector2Score", 0)
	writeData("restussPointControl:sector3Score", 0)
	
	writeData("restussPointControl:overallScore", 0)
	
	writeData("restussPointControl:battleTimer", 26)

	createEvent(8 * 1000, self.scriptName, "doControlPulse", nil, "")
	createEvent(2 * 60 * 1000, self.scriptName, "doScorePulse", nil, "")
	createEvent(1739 * 1000, self.scriptName, "rewardWinner", nil, "") -- 27 minutes 59 seconds. Avoids overlap with score pulse. 2 minutes 1 second respawn time.
end

---- Change Flag Mechanic
function restussPointControl:changeFlag(pSector, pScore)
	local banner = ""

	if (pScore == 0) then 
		writeStringData("restussPointControl:banner", "object/tangible/furniture/all/event_flag_game_neut_banner.iff")
	elseif (pScore > 0) then
		writeStringData("restussPointControl:banner", "object/tangible/furniture/all/event_flag_game_imp_banner.iff")
	elseif (pScore < 0) then
		writeStringData("restussPointControl:banner", "object/tangible/furniture/all/event_flag_game_reb_banner.iff")
	end

	banner = readStringData("restussPointControl:banner")

	local pSector1 = getSceneObject(readData("restussPointControl:pSector1"))
	local pSector2 = getSceneObject(readData("restussPointControl:pSector2"))
	local pSector3 = getSceneObject(readData("restussPointControl:pSector3"))

	if (pSector == 1) then
		self:destroyObject(pSector1)
		pSector = spawnSceneObject(self.planetName, banner, self.x1, self.z1, self.y1, 0, 90)
		writeData("restussPointControl:pSector1", SceneObject(pSector):getObjectID())
		self:storeObjectID(pSector1)
	end

	if (pSector == 2) then
		self:destroyObject(pSector2)
		pSector = spawnSceneObject(self.planetName, banner, self.x2, self.z2, self.y2, 0, 90)
		writeData("restussPointControl:pSector2", SceneObject(pSector):getObjectID())
		self:storeObjectID(pSector2)
	end

	if (pSector == 3) then
		self:destroyObject(pSector3)
		pSector = spawnSceneObject(self.planetName, banner, self.x3, self.z3, self.y3, 0, 90)
		writeData("restussPointControl:pSector3", SceneObject(pSector):getObjectID())
		self:storeObjectID(pSector3)
	end

	return

end

function restussPointControl:alterControl(pSector, sectorPlayers)
	if (pSector == nil) then
		print("Sector Nil")
		return
	end

	if (sectorPlayers == nil) then
		print ("Players Nil")
		return
	end

	if (sectorPlayers > 1) then
		sectorPlayers = 1
	elseif (sectorPlayers < 1) then
		sectorPlayers = -1
	end

	if (pSector == 1) then
		local oldScore = readData("restussPointControl:sector1Score")
		local pScore = oldScore + sectorPlayers
		if (pScore > 3) then
			pScore = 3
		elseif (pScore < -3) then
		pScore = -3
		end
		self:notifyPlayers(oldScore, pScore, "The Main Gate")
		writeData("restussPointControl:sector1Score", pScore)
		if (pScore > 3) then
			pScore = 3
		end
		if (pScore > 1) then
			return
		end
		if (pScore < -1) then
			return
		end
		self:changeFlag(pSector, pScore)
	elseif (pSector == 2) then
		local oldScore = readData("restussPointControl:sector2Score")
		local pScore = oldScore + sectorPlayers
		if (pScore > 3) then
			pScore = 3
		elseif (pScore < -3) then
		pScore = -3
		end
		self:notifyPlayers(oldScore, pScore, "Starport")
		writeData("restussPointControl:sector2Score", pScore)
		if (pScore > 1) then
			return
		end
		if (pScore < -1) then
			return
		end
		self:changeFlag(pSector, pScore)
	elseif (pSector == 3) then
		local oldScore = readData("restussPointControl:sector3Score")
		local pScore = oldScore + sectorPlayers
		if (pScore > 3) then
			pScore = 3
		elseif (pScore < -3) then
		pScore = -3
		end
		self:notifyPlayers(oldScore, pScore, "The Grounded Gunship")
		writeData("restussPointControl:sector3Score", pScore)
		if (pScore > 1) then
			return
		end
		if (pScore < -1) then
			return
		end
		self:changeFlag(pSector, pScore)
	end

	return

end

---- Area Control Mechanic
function restussPointControl:doControlPulse()
	local despawn = readData("restussPointControl:despawn")
	if (despawn == 1) then
		print ("Event Despawned")
		return
	end
	local pSector1 = getSceneObject(readData("restussPointControl:pSector1"))
	local pSector2 = getSceneObject(readData("restussPointControl:pSector2"))
	local pSector3 = getSceneObject(readData("restussPointControl:pSector3"))
	
	if (pSector1 == nil) then
		print("Sector 1 Null")
		return
	elseif (pSector2 == nil) then
		print("Sector 2 Null")
		return
	elseif (pSector3 == nil) then
		print("Sector 1 Null")
		return
	end
		
	local sector1Players = self:countPlayers(pSector1)
	local sector2Players = self:countPlayers(pSector2)
	local sector3Players = self:countPlayers(pSector3)
	
	if (sector1Players ~= 0) then
		self:alterControl (1, sector1Players, pSector1)
	end
	
	if (sector2Players ~= 0) then
		self:alterControl (2, sector2Players, pSector2)
	end
	
	if (sector3Players ~= 0) then
		self:alterControl (3, sector3Players, pSector3)
	end
	
	local sector1Score = readData("restussPointControl:sector1Score")
	local sector2Score = readData("restussPointControl:sector2Score")
	local sector3Score = readData("restussPointControl:sector3Score")
	local overallScore = readData("restussPointControl:overallScore")
	
	overallScore = overallScore + sector1Score + sector2Score + sector3Score
	
	writeData("restussPointControl:overallScore", overallScore)
	
	createEvent(8 * 1000, self.scriptName, "doControlPulse", nil, "")

end

function restussPointControl:countPlayers(pSector)
	local radius = 25
	local score = 0
	local playerTable = SceneObject(pSector):getPlayersInRange(radius)

	if (playerTable == nil) then
		return
	end

	for i = 1, #playerTable, 1 do
		local pPlayer = playerTable[i]
		
		CreatureObject(pPlayer):enhanceCharacter()

		if (pPlayer ~= nil) then
			if (not CreatureObject(pPlayer):isDead()) then
				if (ThemeParkLogic:isInFaction(FACTIONIMPERIAL, pPlayer) == true) then
					score = score + 1
				end
				if (ThemeParkLogic:isInFaction(FACTIONREBEL, pPlayer) == true) then
					score = score - 1
				end
			end
		end
	end
	return score
end

function restussPointControl:notifyPlayers(oldScore, newScore, message)
	local pSector = getSceneObject(readData("restussPointControl:pSector2"))
	local radius = 500
	local faction = ""
	local strength = ""
	
	if (oldScore == newScore) then
		return
	end
	
	if (newScore < 0) then
		if (oldScore == 0) then
			faction = "The Confederacy has gained control of "
		elseif (oldScore < newScore) then
			faction = "The Confederacy is losing control of "
		else
			faction = "The Confederacy is increasing their control of "
		end
	end
	
	if (newScore > 0) then
		if (oldScore == 0) then
			faction = "The Republic has gained control of "
		elseif (oldScore > newScore) then
			faction = "The Republic is losing control of "
		else
			faction = "The Republic is increasing their control of "
		end
	end
	
	if (newScore == 0) then
		if (newScore < oldScore) then
			faction = "The Republic has lost control of "
		else
			faction = "The Confederacy has lost control of "
		end
	end
	
	if (newScore == 3 or newScore == -3) then
		strength = ". They have total control."
	elseif (newScore == 2 or newScore == -2) then
		strength = ". They have significant control."
	elseif (newScore == 1 or newScore == -1) then
		strength = ". They have limited control."
	else
		strength = "."
	end
	
	local playerTable = SceneObject(pSector):getPlayersInRange(radius)

	if (playerTable == nil) then
		return
	end

	for i = 1, #playerTable, 1 do
		local pPlayer = playerTable[i]
		if (pPlayer ~= nil) then
			CreatureObject(pPlayer):sendSystemMessage(faction .. message .. strength)
		end
	end
end

function restussPointControl:doScorePulse()
	local pSector = getSceneObject(readData("restussPointControl:pSector2"))
	local despawn = readData("restussPointControl:despawn")
	if (despawn == 1) then
		print ("Event Despawned")
		return
	end

	local overallScore = readData("restussPointControl:overallScore")
	local radius = 500
	local message = ""
	local faction = ""
	local battleTimer = readData("restussPointControl:battleTimer")
	
	local playerTable = SceneObject(pSector):getPlayersInRange(radius)

	if (playerTable == nil) then
		return
	end
	
	if (overallScore < 0) then
		message = "The Current Restuss Score is: "
		faction = ". The Confederacy control the city."
	elseif (overallScore > 0) then
		message = "The Current Restuss Score is: "
		faction = ". The Republic control the city."
	elseif (overallScore == 0) then
		message = "The Current Restuss Score is tied at "
		faction = ". The city is contested."
	end

	for i = 1, #playerTable, 1 do
		local pPlayer = playerTable[i]
		if (pPlayer ~= nil) then
			CreatureObject(pPlayer):sendSystemMessage(message .. overallScore .. faction)
			CreatureObject(pPlayer):sendSystemMessage(battleTimer .. " minutes remain.")
		end
	end
	
	battleTimer = (battleTimer - 2)
	writeData("restussPointControl:battleTimer", battleTimer)
		
	createEvent(2 * 60 * 1000, self.scriptName, "doScorePulse", nil, "")

end

function restussPointControl:rewardWinner()

	local pSector = getSceneObject(readData("restussPointControl:pSector2"))
	local overallScore = readData("restussPointControl:overallScore")
	
	if (pSector == nil) then
		return
	end
	
	local playerTable = SceneObject(pSector):getPlayersInRange(500)

	if (playerTable == nil) then
		return
	end
	
	for i = 1, #playerTable, 1 do
		local pPlayer = playerTable[i]

		if (pPlayer ~= nil) then
			if (not CreatureObject(pPlayer):isDead()) then
				if (ThemeParkLogic:isInFaction(FACTIONIMPERIAL, pPlayer) == true) then
						if (overallScore > 0) then
							CreatureObject(pPlayer):sendSystemMessage("You have won the battle for Restuss!")
							self:giveAwards(pPlayer)
							self:giveAwards(pPlayer)
							self:giveAwards(pPlayer)
							self:giveAwards(pPlayer)
							self:giveAwards(pPlayer)
						elseif (overallScore < 0) then
							CreatureObject(pPlayer):sendSystemMessage("You have lost the battle for Restuss!")
						else
							CreatureObject(pPlayer):sendSystemMessage("The battle has ended in a draw.")
						end
				end
				if (ThemeParkLogic:isInFaction(FACTIONREBEL, pPlayer) == true) then
					if (overallScore < 0) then
						CreatureObject(pPlayer):sendSystemMessage("You have won the battle for Restuss!")
						self:giveAwards(pPlayer)
						self:giveAwards(pPlayer)
						self:giveAwards(pPlayer)
						self:giveAwards(pPlayer)
						self:giveAwards(pPlayer)
					elseif (overallScore > 0) then
						CreatureObject(pPlayer):sendSystemMessage("You have lost the battle for Restuss!")
					else
						CreatureObject(pPlayer):sendSystemMessage("The battle has ended in a draw.")
					end
				end
			end
		end
	end
	self:cleanupScene()
	createEvent(121 * 1000, self.scriptName, "startEncounter", nil, "")
end

---- Utilities
-- Cleanup
function restussPointControl:destroyObject(pObject)
	if (pObject == nil) then
		return
	end

	if (SceneObject(pObject):isAiAgent()) then
		CreatureObject(pObject):setPvpStatusBitmask(0)
		forcePeace(pObject)
	end
	SceneObject(pObject):destroyObjectFromWorld()
end

function restussPointControl:cleanupScene()
	writeData("restussPointControl:despawn", 1)
	
	local pSector1 = getSceneObject(readData("restussPointControl:pSector1"))
	local pSector2 = getSceneObject(readData("restussPointControl:pSector2"))
	local pSector3 = getSceneObject(readData("restussPointControl:pSector3"))
	self:destroyObject(pSector1)
	self:destroyObject(pSector2)
	self:destroyObject(pSector3)
	
	local spawnedObjects = readStringData("restussPointControl:spawnedObjects")
	local spawnedObjectsTable = HelperFuncs:splitString(spawnedObjects, ",")
	for i = 1, #spawnedObjectsTable, 1 do
		local pObject = getSceneObject(tonumber(spawnedObjectsTable[i]))
		if (pObject ~= nil) then
			SceneObject(pObject):destroyObjectFromWorld()
		end
	end

	deleteStringData("restussPointControl:spawnedObjects")
	deleteStringData("restussPointControl:banner")
	deleteData("restussPointControl:pSector1")
	deleteData("restussPointControl:pSector2")
	deleteData("restussPointControl:pSector3")
	deleteData("restussPointControl:sector1Score")
	deleteData("restussPointControl:sector2Score")
	deleteData("restussPointControl:sector3Score")
	deleteData("restussPointControl:overallScore")
end

-- Store spawned objects
function restussPointControl:storeObjectID(pObject)
	if (pObject == nil) then
		return
	end
	local objectID = SceneObject(pObject):getObjectID()
	writeStringData("restussPointControl:spawnedObjects", readStringData("restussPointControl:spawnedObjects") .. "," .. objectID)
end

-- Fisher-Yates shuffle
function restussPointControl:shuffleTable(tInput)
	math.randomseed(os.time())
	local tReturn = {}
	for i = #tInput, 1, -1 do
		local j = math.random(i)
		tInput[i], tInput[j] = tInput[j], tInput[i]
		table.insert(tReturn, tInput[i])
	end
	return tReturn
end

function restussPointControl:giveAwards(pPlayer)
				
	local pGhost = CreatureObject(pPlayer):getPlayerObject()
	if (pGhost ~= nil and not PlayerObject(pGhost):hasBadge(168)) then
		PlayerObject(pGhost):awardBadge(168)
	end
	
	local pInventory = SceneObject(pPlayer):getSlottedObject("inventory")
	
	rngloot = 0 + getRandomNumber(0, 6)

	if (pInventory == nil or SceneObject(pInventory):isContainerFullRecursive()) then
		CreatureObject(pPlayer):sendSystemMessage("Your inventory is full so you did not receive the battle reward.")
	else
			local crystalID = createLoot(pInventory, "aotc_pvp_tokens", 1, true)
			CreatureObject(pPlayer):sendSystemMessage("You have been awarded a token for your efforts. Maybe you can use it at the Starport Vendor. You store it in your inventory.")
	end
end
