
object_tangible_loot_loot_schematic_proton_carbine_schematic = object_tangible_loot_loot_schematic_shared_proton_carbine_schematic:new {
	templateType = LOOTSCHEMATIC,
	objectMenuComponent = "LootSchematicMenuComponent",
	attributeListComponent = "LootSchematicAttributeListComponent",
	requiredSkill = "crafting_weaponsmith_master",
	targetDraftSchematic = "object/draft_schematic/weapon/carbine_proton.iff",
	targetUseCount = 3
}

ObjectTemplates:addTemplate(object_tangible_loot_loot_schematic_proton_carbine_schematic, "object/tangible/loot/loot_schematic/proton_carbine_schematic.iff")
