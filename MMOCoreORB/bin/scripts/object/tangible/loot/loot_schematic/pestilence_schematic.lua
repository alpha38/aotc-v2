object_tangible_loot_loot_schematic_pestilence_schematic = object_tangible_loot_loot_schematic_shared_pestilence_schematic:new {
	templateType = LOOTSCHEMATIC,
	objectMenuComponent = "LootSchematicMenuComponent",
	attributeListComponent = "LootSchematicAttributeListComponent",
	requiredSkill = "crafting_weaponsmith_master",
	targetDraftSchematic = "object/draft_schematic/weapon/loot_pestilence.iff",
	targetUseCount = 3
}

ObjectTemplates:addTemplate(object_tangible_loot_loot_schematic_pestilence_schematic, "object/tangible/loot/loot_schematic/pestilence_schematic.iff")