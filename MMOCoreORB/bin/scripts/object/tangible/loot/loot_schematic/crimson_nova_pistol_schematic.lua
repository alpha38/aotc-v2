
object_tangible_loot_loot_schematic_crimson_nova_pistol_schematic = object_tangible_loot_loot_schematic_shared_crimson_nova_pistol_schematic:new {
	templateType = LOOTSCHEMATIC,
	objectMenuComponent = "LootSchematicMenuComponent",
	attributeListComponent = "LootSchematicAttributeListComponent",
	requiredSkill = "crafting_weaponsmith_master",
	targetDraftSchematic = "object/draft_schematic/weapon/pistol_pvp.iff",
	targetUseCount = 3
}

ObjectTemplates:addTemplate(object_tangible_loot_loot_schematic_crimson_nova_pistol_schematic, "object/tangible/loot/loot_schematic/crimson_nova_pistol_schematic.iff")
