world_telrissa_peko = Creature:new {
	objectName = "@mob/creature_names:peko_peko_albatross",
	customName = "Telrissa (Peko Peko Form)",
	socialGroup = "nightsister",
	faction = "nightsister",
	mobType = MOB_CARNIVORE,
	level = 155,
	chanceHit = 7.75,
	damageMin = 945,
	damageMax = 1600,
	baseXp = 14694,
	baseHAM = 174000,
	baseHAMmax = 191000,
	armor = 2,
	resists = {200,180,130,180,180,190,25,25,-1},
	meatType = "meat_avian",
	meatAmount = 400,
	hideType = "hide_wooly",
	hideAmount = 120,
	boneType = "bone_avian",
	boneAmount = 100,
	milk = 0,
	tamingChance = 0,
	ferocity = 8,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,

	templates = {"object/mobile/peko_peko_hue.iff"},
	hues = { 16, 17, 18, 19, 20, 21, 22, 23 },
	scale = 2.0,
	lootGroups = {
		{
			groups = {
				{group = "peko_albatross", chance = 10000000}
			},
			lootChance = 8100000
		}
	},

	-- Primary and secondary weapon should be different types (rifle/carbine, carbine/pistol, rifle/unarmed, etc)
	-- Unarmed should be put on secondary unless the mobile doesn't use weapons, in which case "unarmed" should be put primary and "none" as secondary
	primaryWeapon = "unarmed",
	secondaryWeapon = "none",
	conversationTemplate = "",
	
	-- primaryAttacks and secondaryAttacks should be separate skill groups specific to the weapon type listed in primaryWeapon and secondaryWeapon
	-- Use merge() to merge groups in creatureskills.lua together. If a weapon is set to "none", set the attacks variable to empty brackets
	primaryAttacks = { {"creatureareacombo",""}, {"creatureareaknockdown",""} },
	secondaryAttacks = { }
}

CreatureTemplates:addCreatureTemplate(world_telrissa_peko, "world_telrissa_peko")
