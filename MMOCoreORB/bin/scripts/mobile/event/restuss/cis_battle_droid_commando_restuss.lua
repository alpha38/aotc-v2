cis_battle_droid_commando_restuss = Creature:new {
	objectName = "@mob/creature_names:cis_battle_droid",
	mobType = MOB_ANDROID,
	customName = "A Battle Droid Commando",
	socialGroup = "rebel",
	faction = "rebel",
	level = 200,
	chanceHit = 0.36,
	damageMin = 270,
	damageMax = 280,
	baseXp = 2637,
	baseHAM = 7200,
	baseHAMmax = 8800,
	armor = 0,
	resists = {30,30,40,30,30,30,30,30,-1},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = ATTACKABLE,
	creatureBitmask = PACK + KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {
		"object/mobile/death_watch_battle_droid.iff"
		},
		lootGroups = {
			{
				groups = {
					{group = "color_crystals", chance = 100000},
					{group = "junk", chance = 3400000},
					{group = "rifles", chance = 1200000},
					{group = "pistols", chance = 1200000},
					{group = "melee_weapons", chance = 1200000},
					{group = "carbines", chance = 1200000},
					{group = "restuss_cis", chance = 500000},
					{group = "stormtrooper_common", chance = 700000},
					{group = "wearables_common", chance = 500000}
				}
			}
		},

		primaryWeapon = "commando_ranged_droid",
		secondaryWeapon = "unarmed",
		conversationTemplate = "",
		reactionStf = "@npc_reaction/battle_droid",
		personalityStf = "@hireling/hireling_military",
		defaultAttack = "battledroiddefaultattack",
		primaryAttacks = merge(commandomaster,carbineermaster,marksmanmaster),
		secondaryAttacks = { }
}

CreatureTemplates:addCreatureTemplate(cis_battle_droid_commando_restuss, "cis_battle_droid_commando_restuss")
