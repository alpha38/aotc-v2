bandit_sword_schematic = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/loot/loot_schematic/bandit_sword_schematic.iff",
	craftingValues = {
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("bandit_sword_schematic", bandit_sword_schematic)
