proton_carbine_schematic = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/loot/loot_schematic/proton_carbine_schematic.iff",
	craftingValues = {
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("proton_carbine_schematic", proton_carbine_schematic)
