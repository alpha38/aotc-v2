veh_power_plant = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/component/vehicle/veh_power_plant.iff",
	craftingValues = {
	{"useCount",2,6,0},
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("veh_power_plant", veh_power_plant)
