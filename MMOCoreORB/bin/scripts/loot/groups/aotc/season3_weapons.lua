season3_weapons = {
	description = "",
	minimumLevel = 0,
	maximumLevel = 0,
	lootItems = {

	{itemTemplate = "crimson_nova_pistol_schematic", weight = 1666667},
	{itemTemplate = "proton_carbine_schematic", weight = 1666667},
	{itemTemplate = "ld1_rifle_schematic", weight = 1666666},
	{itemTemplate = "bandit_sword_schematic", weight = 1666667},
	{itemTemplate = "sickle_2h_sword_schematic", weight = 1666667},
	{itemTemplate = "pestilence_schematic", weight = 1666666}

	}
}

addLootGroupTemplate("season3_weapons", season3_weapons)
