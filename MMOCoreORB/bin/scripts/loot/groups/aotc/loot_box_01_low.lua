--Automatically generated by SWGEmu Spawn Tool v0.12 loot editor.

loot_box_01_low = {
	description = "",
	minimumLevel = 126,
	maximumLevel = 126,
	lootItems = {
	{itemTemplate = "clothing_attachments", weight = 1250000},
	{itemTemplate = "armor_attachments", weight = 1250000},
	{itemTemplate = "janta_blood", weight = 1250000},
	{itemTemplate = "janta_hides", weight = 1250000},
	{itemTemplate = "rancor_bile", weight = 1250000},
	{itemTemplate = "acklay_venom", weight = 1250000},
	{itemTemplate = "geo_spider_venom", weight = 1250000},
	{itemTemplate = "box_junk_100", weight = 1250000}
	}
}

addLootGroupTemplate("loot_box_01_low", loot_box_01_low)
