/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef FORCESERENITY1COMMAND_H_
#define FORCESERENITY1COMMAND_H_

#include "server/zone/objects/player/PlayerObject.h"
#include "server/zone/managers/player/PlayerManager.h"
#include "server/zone/managers/combat/CombatManager.h"
#include "templates/params/creature/CreatureAttribute.h"
#include "server/zone/managers/stringid/StringIdManager.h"
#include "server/zone/managers/collision/CollisionManager.h"
#include "server/zone/managers/frs/FrsManager.h"
#include "server/zone/objects/building/BuildingObject.h"
#include "server/zone/objects/creature/events/ForceSerenityEvent.h"

class ForceSerenity1Command : public JediQueueCommand {
public:

	ForceSerenity1Command(const String& name, ZoneProcessServer* server)
		: JediQueueCommand(name, server) {
			clientEffect = "clienteffect/pl_force_heal_self.cef";
			animationCRC = STRING_HASHCODE("force_healing_1");
	}

	int doQueueCommand(CreatureObject* player, const uint64& target, const UnicodeString& arguments) const {
		int forceCost = 500;
		int cooldownMilli = 30000; // 1min
		int durationSec =  5; // 10 seconds
		unsigned int buffCRC = BuffCRC::FORCE_SERENITY_1;

		if (!checkStateMask(player))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(player))
			return INVALIDLOCOMOTION;

		if (player->isDead() || player->isIncapacitated())
			return INVALIDSTATE;

		ManagedReference<PlayerObject*> ghost = player->getPlayerObject();
		if( ghost == nullptr )
			return GENERALERROR;

		int currentForce = ghost->getForcePower();

		// Check player force
		if (currentForce <= forceCost) {
			player->sendSystemMessage("You do not have enough mental focus to use Serenity"); // "You do not have enough mental focus to use Serenity."
			return GENERALERROR;
		}

		// Check cooldown
		if ( !player->getCooldownTimerMap()->isPast("forceSerenityCooldown") ){
			player->sendSystemMessage("You are not ready to use Force Serenity again"); // "You are not ready to use Force Serenity again"
			return GENERALERROR;
			}
			player->getCooldownTimerMap()->updateToCurrentAndAddMili("forceSerenityCooldown", cooldownMilli);
			player->sendSystemMessage("You are now in a serene state."); // "Your pets fight with renewed vigor"
			
			ghost->setForcePower(currentForce - forceCost);
			
			ManagedReference<Buff*> buff = new Buff(player, buffCRC, durationSec, BuffType::OTHER);
			Locker locker(buff);
			buff->setAttributeModifier(CreatureAttribute::CONSTITUTION, 0);
			player->addBuff(buff);
			
			Reference<ForceSerenityEvent*> heal01 = new ForceSerenityEvent(player);
			Reference<ForceSerenityEvent*> heal02 = new ForceSerenityEvent(player);
			Reference<ForceSerenityEvent*> heal03 = new ForceSerenityEvent(player);
			Reference<ForceSerenityEvent*> heal04 = new ForceSerenityEvent(player);
			Reference<ForceSerenityEvent*> heal05 = new ForceSerenityEvent(player);
			
			player->removePendingTask("heal01");
			player->removePendingTask("heal02");
			player->removePendingTask("heal03");
			player->removePendingTask("heal04");
			player->removePendingTask("heal05");

			//player->playEffect("clienteffect/pl_force_resist_disease_self.cef");
			player->addPendingTask("heal01", heal01, 100);
			player->addPendingTask("heal02", heal02, 1000);
			player->addPendingTask("heal03", heal03, 2000);
			player->addPendingTask("heal04", heal04, 3000);
			player->addPendingTask("heal05", heal05, 4000);

		return SUCCESS;
	}

};

#endif //FORCESERENITY1COMMAND_H_
